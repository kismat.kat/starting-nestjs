// describe('My test', () => {
//   it('returns true', () => {
//     expect(true).toEqual(true);
//   });
// });

class FriendList {
  friends = [];
  addFriend(name: any) {
    this.friends.push(name);
    this.anounceFriendship(name);
  }
  anounceFriendship(name: any) {
    global.console.log(`${name} is now a friend`);
  }
  removeFriend(name) {
    const idx = this.friends.indexOf(name);
    if (idx === -1) {
      throw new Error('Friend not Found!');
    }
    this.friends.splice(idx, 1);
  }
}
// test
describe('FriendList', () => {
  let friendList;
  beforeEach(() => {
    friendList = new FriendList();
  });
  it('initializes friend list', () => {
    expect(friendList.friends.length).toEqual(0);
  });

  it('add a friend to list', () => {
    friendList.addFriend('kat');
    expect(friendList.friends.length).toEqual(1);
  });

  it('anunce friendship', () => {
    friendList.anounceFriendship = jest.fn();
    expect(friendList.anounceFriendship).not.toHaveBeenCalled();
    friendList.addFriend('luffy');
    expect(friendList.anounceFriendship).toHaveBeenCalledWith('luffy');
  });

  describe('removeFriend', () => {
    it('remove a friend to list', () => {
      friendList.addFriend('kuro');
      expect(friendList.friends[0]).toEqual('kuro');
      friendList.removeFriend('kuro');
      expect(friendList.friends[0]).toBeUndefined();
    });
    it('Throw an error as friend dooes not exist in list', () => {
      expect(() => friendList.removeFriend('kuro')).toThrow(
        new Error('Friend not Found!'),
      );
    });
  });
});
