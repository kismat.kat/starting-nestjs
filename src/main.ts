import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
import * as config from 'config';
async function nestjs() {
  const serverConfig = config.get('server');
  const log = new Logger('nestjs');
  const app = await NestFactory.create(AppModule);

  if (process.env.NODE_ENV === 'development') {
    app.enableCors();
  } else {
    app.enableCors({ origin: serverConfig.origin });
    log.log(`Accepting request from ${serverConfig.origin}`);
  }
  const port = process.env.PORT || serverConfig.port;
  await app.listen(serverConfig.port);
  log.log(`Application port listning on port ${port}`);
}
nestjs();
