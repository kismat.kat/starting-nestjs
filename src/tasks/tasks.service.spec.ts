import { NotFoundException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { GetTaskFilterDto } from './dto/get-task-filter-dto';
import { TaskStatus } from './task-status.enum';
import { TaskRepository } from './task.repository';
import { TasksService } from './tasks.service';

const mockUser = { id: 1, username: 'admin' };

const mockTaskRepository = () => ({
  getTasks: jest.fn(),
  findOne: jest.fn(),
  createTask: jest.fn(),
});
describe('TasksService', () => {
  let tasksService;
  let taskRepository;
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        TasksService,
        { provide: TaskRepository, useFactory: mockTaskRepository },
      ],
    }).compile();
    tasksService = await module.get<TasksService>(TasksService);
    taskRepository = await module.get<TaskRepository>(TaskRepository);
  });
  describe('getTasks', () => {
    it('get all tasks from repository', async () => {
      taskRepository.getTasks.mockResolvedValue('someValue');
      expect(taskRepository.getTasks).not.toHaveBeenCalled();

      const filters: GetTaskFilterDto = {
        status: TaskStatus.IN_PROGRESS,
        search: 'some search query',
      };
      // call tasksService.getTasks
      const result = await tasksService.getTasks(filters, mockUser);
      expect(taskRepository.getTasks).toHaveBeenCalled();
      expect(result).toEqual('someValue');
    });
  });

  describe('getTasksById', () => {
    it('calls taskstaskRepository.findOne() and successfully retrives and return the task ', async () => {
      const mocTask = {
        title: 'Test task',
        description: 'a test desc',
      };
      taskRepository.findOne.mockResolvedValue(mocTask);
      const result = await tasksService.getSelectedTasks(1, mockUser);
      expect(result).toEqual(mocTask);
      expect(taskRepository.findOne).toHaveBeenCalledWith({
        where: {
          id: 1,
          userId: mockUser.id,
        },
      });
    });
    it('Throws an error as task is not found ', async () => {
      taskRepository.findOne.mockResolvedValue(null);
      expect(taskRepository.findOne(1, mockUser)).rejects.toThrow(
        NotFoundException,
      );
    });
  });

  describe('createTask', () => {
    it('calls taskstaskRepository.createTask() and return the result ', async () => {
      taskRepository.createTask.mockResolvedValue('someTask');
      const createTaskDto = {
        title: 'Test task',
        description: 'a test desc',
      };
      expect(taskRepository.createTask).not.toHaveBeenCalled();
      const result = await tasksService.createTask(createTaskDto, mockUser);
      expect(taskRepository.createTask).toHaveBeenCalledWith(
        createTaskDto,
        mockUser,
      );
      expect(result).toEqual('someTask');
    });
  });
  describe('updateTasks', () => {
    it('update a task status ', async () => {
      const save = jest.fn().mockResolvedValue(true);
      const createTaskDto = { title: 'kat', description: 'test' };
      taskRepository.getSelectedTasks = jest.fn().mockResolvedValue({
        status: TaskStatus.OPEN,
        save,
      });
      expect(taskRepository.getSelectedTasks).not.toHaveBeenCalled();
      expect(save).not.toHaveBeenCalled();
      const result = await tasksService.updateTasks(
        1,
        TaskStatus.DONE,
        createTaskDto,
        mockUser,
      );
      expect(taskRepository.getSelectedTasks).toHaveBeenCalled();
      expect(save).toHaveBeenCalled();
      expect(result.status).toEqual(TaskStatus.DONE);
    });
  });
});
