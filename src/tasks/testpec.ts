// import { Test, TestingModule } from '@nestjs/testing';
// import { SaSubscriptionService } from './sa-subscription.service';
// import { SaChargeTemplates } from '@entities/SaChargeTemplates';
// import { SaSubscription } from '@entities/SaSubscription';
// import { SaSubscriptionTemp } from '@entities/SaSubscriptionTemp';
// import { getRepositoryToken } from '@nestjs/typeorm';
// import { Repository } from 'typeorm';
// import { HttpException } from '@nestjs/common';
// import { CreateSaSubscriptionDto } from '@dtos/CreateSaSubscription.dto';
// import { UpdateSaSubscriptionDto } from '@dtos/UpdateSaSubscription.dto';
// import { ApproveRejectDto } from '@dtos/ApproveReject.dto';
// import {
//   cleanData,
//   isFutureDate,
//   isValidDate,
//   paginate,
//   removeEmpty,
//   validateUUID,
// } from '@utils/helpers';
// import { doesNotMatch } from 'assert';

// const defaultSubscriptions = [
//   {
//     idx: '0836E005-AC2F-EB11-9FB4-00155D03FF66',
//     name: 'Test Saplicy',
//   },
// ];

// const allSubscriptions = {
//   total_pages: 1,
//   total_items: 1,
//   next: '?page=11',
//   previous: '',
//   current_page: '1',
//   items: [
//     {
//       id: 1,
//       idx: '58C41DDA-042E-EB11-9FB4-00155D03FF66',
//       name: 'Default',
//       base_period: 'WEEKLY',
//       expires_on: null,
//       no_of_free_transaction: 0,
//       is_default_subscription: false,
//       charge_value: 1,
//       charge_payer: 'EMPLOYEE',
//       is_active: true,
//       is_obsolete: false,
//       created_on: '2020-11-24T03:26:34.910Z',
//       created_by: 'B25C17EB-4E2D-EB11-9FB4-00155D03FF66',
//       modified_on: '2020-11-24T03:26:34.910Z',
//       modified_by: null,
//       is_on_edit: false,
//       sa_default_subscription: null,
//     },
//   ],
// };

// const oneSubscription = {
//   id: 1,
//   idx: '58C41DDA-042E-EB11-9FB4-00155D03FF66',
//   name: 'Default',
//   base_period: 'WEEKLY',
//   expires_on: null,
//   no_of_free_transaction: 0,
//   is_default_subscription: false,
//   charge_value: 1,
//   charge_payer: 'EMPLOYEE',
//   is_active: true,
//   is_obsolete: false,
//   created_on: '2020-11-24T03:26:34.910Z',
//   created_by: 'B25C17EB-4E2D-EB11-9FB4-00155D03FF66',
//   modified_on: '2020-11-24T03:26:34.910Z',
//   modified_by: null,
//   is_on_edit: false,
// };

// describe('ChargeService', () => {
//   let service: SaSubscriptionService;
//   let saChargeTemplatesRepo: Repository<SaChargeTemplates>;
//   let saSubscriptionRepo: Repository<SaSubscription>;
//   let saSubscriptionTempRepo: Repository<SaSubscriptionTemp>;

//   beforeEach(async () => {
//     // increasing timeout as some async tasks may take longer
//     jest.setTimeout(30000);
//     jest.clearAllMocks();
//     const module: TestingModule = await Test.createTestingModule({
//       providers: [
//         SaSubscriptionService,
//         {
//           provide: getRepositoryToken(SaSubscription),
//           useValue: {
//             find: jest.fn().mockResolvedValue(defaultSubscriptions),
//             findOne: jest.fn(),
//           },
//         },
//         {
//           provide: getRepositoryToken(SaSubscriptionTemp),
//           useValue: {
//             find: jest.fn(),
//             findOne: jest.fn(),
//           },
//         },
//         {
//           provide: getRepositoryToken(SaChargeTemplates),
//           useValue: {
//             find: jest.fn(),
//             findOne: jest.fn(),
//           },
//         },
//       ],
//     }).compile();

//     service = module.get<SaSubscriptionService>(SaSubscriptionService);
//     // saChargeTemplatesRepo = module.get<Repository<SaChargeTemplates>>(
//     //   getRepositoryToken(SaChargeTemplates),
//     // );
//     // saSubscriptionTempRepo = module.get<Repository<SaSubscriptionTemp>>(
//     //   getRepositoryToken(SaSubscriptionTemp),
//     // );
//     saSubscriptionRepo = module.get<Repository<SaSubscription>>(
//       getRepositoryToken(SaSubscription),
//     );
//   });

//   it('should be defined', () => {
//     expect(service).toBeDefined();
//   });

//   describe('getDefaultSubscriptions', () => {
//     it('should getDefaultSubscriptions', async () => {
//       try {
//         const getDefaultSubscription = service.getDefaultSubscriptions();

//         expect(getDefaultSubscription).resolves.toEqual(defaultSubscriptions);
//       } catch (error) {
//         expect(error).toBeInstanceOf(HttpException);
//       }
//     });
//     it('should not get empty object only', async () => {
//       const getDefaultSubscription = service.getDefaultSubscriptions();

//       expect(getDefaultSubscription).resolves.not.toEqual({});
//     });
//   });

//   describe('get all subscription', () => {
//     it('should get all subscription', async () => {
//       try {
//         let idx: string;
//         const page = 1;
//         const offset = 0;
//         const limit = 10;
//         const request_type = '';
//         const order_by = '';
//         const search = '';
//         const getAllSub = service.getAll(
//           idx,
//           page,
//           offset,
//           limit,
//           request_type,
//           order_by,
//           search,
//         );

//         expect(getAllSub).resolves.toEqual(allSubscriptions);
//       } catch (error) {
//         expect(error).toBeInstanceOf(HttpException);
//       }
//     });
//     it('should not get empty object only', async () => {
//       let idx: string;
//       const page = 1;
//       const offset = 0;
//       const limit = 10;
//       const request_type = '';
//       const order_by = '';
//       const search = '';
//       const getAllSub = service.getAll(
//         idx,
//         page,
//         offset,
//         limit,
//         request_type,
//         order_by,
//         search,
//       );

//       expect(getAllSub).resolves.not.toEqual({});
//     });
//   });

//   describe('get all pending subscription', () => {
//     it('should get all pending subscription', async () => {
//       try {
//         let idx: string;
//         const page = 1;
//         const offset = 0;
//         const limit = 10;
//         const request_type = '';
//         const order_by = '';
//         const search = '';
//         const getAllSub = service.getAll(
//           idx,
//           page,
//           offset,
//           limit,
//           request_type,
//           order_by,
//           search,
//         );

//         expect(getAllSub).resolves.toEqual(allSubscriptions);
//       } catch (error) {
//         expect(error).toBeInstanceOf(HttpException);
//       }
//     });
//     it('should not get empty object only', async () => {
//       let idx: string;
//       const page = 1;
//       const offset = 0;
//       const limit = 10;
//       const request_type = '';
//       const order_by = '';
//       const search = '';
//       const getAllSub = service.getAll(
//         idx,
//         page,
//         offset,
//         limit,
//         request_type,
//         order_by,
//         search,
//       );

//       expect(getAllSub).resolves.not.toEqual({});
//     });
//   });

//   describe('get one pending subscription', () => {
//     it('should get one pending subscription', async () => {
//       try {
//         let idx: string;
//         const getOne = service.getPendingSubscriptionById(idx);

//         expect(getOne).resolves.toEqual(oneSubscription);
//       } catch (error) {
//         expect(error).toBeInstanceOf(HttpException);
//       }
//     });
//     it('should not get empty object only', async () => {
//       let idx: string;
//       const getOne = service.getPendingSubscriptionById(idx);

//       expect(getOne).resolves.not.toEqual({});
//     });
//   });

//   describe('get one subscription', () => {
//     it('should get one subscription', async () => {
//       try {
//         let idx: string;
//         const getOne = service.getOne(idx);

//         expect(getOne).resolves.toEqual(oneSubscription);
//       } catch (error) {
//         expect(error).toBeInstanceOf(HttpException);
//       }
//     });
//     it('should not get empty object only', async () => {
//       let idx: string;
//       const getOne = service.getOne(idx);

//       expect(getOne).resolves.not.toEqual({});
//     });
//   });

//   describe('create one subscription', () => {
//     it('should create one subscription', async () => {
//       try {
//         let userIdx: string;
//         const dto: CreateSaSubscriptionDto = {
//           name: 'Test',
//           base_period: 'DAILY',
//           no_of_free_transaction: 1,
//           is_default_subscription: true,
//           charge_payer: 'EMPLOYEE',
//           charge_value: 100,
//         };

//         if (dto.is_default_subscription) {
//           if (dto.expires_on) {
//             expect(service.createSubscription(userIdx, dto)).toBeInstanceOf(
//               HttpException,
//             );
//           }
//           dto.expires_on = null;

//           if (dto.default_subscription_idx) {
//             expect(service.createSubscription(userIdx, dto)).toBeInstanceOf(
//               HttpException,
//             );
//           }
//         } else {
//           if (!dto.expires_on) {
//             expect(service.createSubscription(userIdx, dto)).toBeInstanceOf(
//               HttpException,
//             );
//           }

//           if (!isValidDate(dto.expires_on)) {
//             expect(service.createSubscription(userIdx, dto)).toBeInstanceOf(
//               HttpException,
//             );
//           }

//           if (!isFutureDate(dto.expires_on)) {
//             expect(service.createSubscription(userIdx, dto)).toBeInstanceOf(
//               HttpException,
//             );
//           }

//           if (!dto.default_subscription_idx) {
//             expect(service.createSubscription(userIdx, dto)).toBeInstanceOf(
//               HttpException,
//             );
//           }
//         }
//         const createOne = service.createSubscription(userIdx, dto);

//         expect(createOne).resolves.toEqual({
//           statusCode: 201,
//           message: 'Subscription added',
//         });
//       } catch (error) {
//         expect(error).toBeInstanceOf(HttpException);
//       }
//     });
//     it('should not get empty object only', async () => {
//       let userIdx: string;
//       let dto: CreateSaSubscriptionDto;
//       const createOne = service.createSubscription(userIdx, dto);

//       expect(createOne).resolves.not.toEqual({});
//     });
//   });

//   describe('update one subscription', () => {
//     it('should update one subscription', async () => {
//       try {
//         const subscriptionId = '0836E005-AC2F-EB11-9FB4-00155D03FF66';
//         const userId = '0836E005-AC2F-EB11-9FB4-00155D03FF67';
//         const dto: UpdateSaSubscriptionDto = {
//           name: 'Test',
//           base_period: 'DAILY',
//           no_of_free_transaction: 1,
//           is_default_subscription: true,
//           charge_payer: 'EMPLOYEE',
//           charge_value: 100,
//         };

//         const updateOne = service.updateSaSubscription(
//           subscriptionId,
//           userId,
//           dto,
//         );

//         expect(updateOne).resolves.toEqual({
//           statusCode: 200,
//           message: 'Subscription updated and waiting for approval',
//         });
//       } catch (error) {
//         expect(error).toBeInstanceOf(HttpException);
//       }
//     });
//     it('should not get empty object only', async () => {
//       const subscriptionId = '0836E005-AC2F-EB11-9FB4-00155D03FF66';
//       const userId = '0836E005-AC2F-EB11-9FB4-00155D03FF67';
//       const dto: UpdateSaSubscriptionDto = {
//         name: 'Test',
//         base_period: 'DAILY',
//         no_of_free_transaction: 1,
//         is_default_subscription: true,
//         charge_payer: 'EMPLOYEE',
//         charge_value: 100,
//       };
//       const updateOne = service.updateSaSubscription(
//         subscriptionId,
//         userId,
//         dto,
//       );

//       expect(updateOne).resolves.not.toEqual({});
//     });
//   });

//   describe('verify one subscription', () => {
//     it('should verify one subscription', async () => {
//       try {
//         let dto: ApproveRejectDto;
//         let subscriptionIdx: string;
//         let userIdx: string;

//         const updateOne = service.verify(dto, subscriptionIdx, userIdx);

//         expect(updateOne).resolves.toEqual({
//           statusCode: 200,
//           message: 'Verified Delete Operation',
//         });
//       } catch (error) {
//         expect(error).toBeInstanceOf(HttpException);
//       }
//     });
//     it('should not get empty object only', async () => {
//       let dto: ApproveRejectDto;
//       let subscriptionIdx: string;
//       let userIdx: string;

//       const verifyOne = service.verify(dto, subscriptionIdx, userIdx);

//       expect(verifyOne).resolves.not.toEqual({});
//     });
//   });

//   describe('delete one subscription', () => {
//     it('should delete one subscription', async () => {
//       try {
//         let idx: string;

//         const deleteOne = service.deleteCharge(idx);

//         expect(deleteOne).resolves.toEqual({
//           statusCode: 200,
//           message: 'Charge deleted',
//         });
//       } catch (error) {
//         expect(error).toBeInstanceOf(HttpException);
//       }
//     });
//     it('should not get empty object only', async () => {
//       let idx: string;

//       const deleteOne = service.deleteCharge(idx);

//       expect(deleteOne).resolves.not.toEqual({});
//     });
//   });
// });